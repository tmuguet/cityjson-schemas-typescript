import fs from 'fs';
import path from 'path';
import { URL, fileURLToPath } from 'url';
import { compile } from 'json-schema-to-typescript';
import { glob } from 'glob';

const baseDir = path.dirname(fileURLToPath(import.meta.url));

const cityJsonEntries = {
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.1/cityjson-v01.schema.json': 'CityJSONV0_1',
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.2/cityjson-v02.schema.json': 'CityJSONV0_2',
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.3/cityjson-v03.schema.json': 'CityJSONV0_3',
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.5/cityjson-v05.schema.json': 'CityJSONV0_5',
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.6/cityjson.schema.json': 'CityJSONV0_6',
    'https://3d.bk.tudelft.nl/schemas/cityjson/0.9/cityjson.schema.json': 'CityJSONV0_9',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.0.0/cityjson.schema.json': 'CityJSONV1_0_0',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.0.1/cityjson.schema.json': 'CityJSONV1_0_1',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.0.2/cityjson.schema.json': 'CityJSONV1_0_2',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.0.3/cityjson.schema.json': 'CityJSONV1_0_3',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.0/cityjson.schema.json': 'CityJSONV1_1_0',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.1/cityjson.min.schema.json': 'CityJSONV1_1_1',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.2/cityjson.schema.json': 'CityJSONV1_1_2',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.3/cityjson.schema.json': 'CityJSONV1_1_3',
    'https://3d.bk.tudelft.nl/schemas/cityjson/2.0.0/cityjson.schema.json': 'CityJSONV2_0_0',
    'https://3d.bk.tudelft.nl/schemas/cityjson/2.0.1/cityjson.schema.json': 'CityJSONV2_0_1',
}

const cityJsonFeatureEntries = {
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.0/cityjsonfeature.schema.json': 'CityJSONFeatureV1_1_0',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.1/cityjsonfeature.schema.json': 'CityJSONFeatureV1_1_1',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.2/cityjsonfeature.schema.json': 'CityJSONFeatureV1_1_2',
    'https://3d.bk.tudelft.nl/schemas/cityjson/1.1.3/cityjsonfeature.schema.json': 'CityJSONFeatureV1_1_3',
    'https://3d.bk.tudelft.nl/schemas/cityjson/2.0.0/cityjsonfeature.schema.json': 'CityJSONFeatureV2_0_0',
    'https://3d.bk.tudelft.nl/schemas/cityjson/2.0.1/cityjsonfeature.schema.json': 'CityJSONFeatureV2_0_1',
};

const entries = Object.entries(cityJsonEntries).concat(Object.entries(cityJsonFeatureEntries));

const genExport = (name, aliases) => `export type ${name} = `
    + aliases.map(name => `${name}.${name}`).join(" | ")
    + ";";

const genCityJsonVersionExport = (version) => genExport(
    `CityJSONV${version}_X`,
    Object.values(cityJsonEntries).filter(name => name.startsWith(`CityJSONV${version}_`))
);

const genCityJsonFeatureVersionExport = (version) => genExport(
    `CityJSONFeatureV${version}_X`,
    Object.values(cityJsonFeatureEntries).filter(name => name.startsWith(`CityJSONFeatureV${version}_`))
);

// Clean old definitions
glob.sync(`${baseDir}/*.d.ts`).forEach(file => fs.rmSync(file));

for (const [inputFile, outputName] of entries) {
    // const inputFullpath = path.resolve(inputFile);
    // const inputBaseDir = path.dirname(inputFullpath);

    const url = new URL(inputFile);
    const parts = url.href.split('/');
    parts.splice(-1, 1);

    const basePath = parts.join('/');
    const outputFile = `${outputName}.d.ts`;

    console.log(`Fetching ${inputFile}`);
    const response = await fetch(inputFile);
    const content = await response.json();

    const resolver = {
        order: 1,
        canRead: true,
    
        async read(file, $refs) {
            const filename = path.relative(process.cwd(), file.url);
            const url = `${basePath}/${filename}`;
            console.log(`Fetching ${url}`);
            const response = await fetch(url);
            return response.json();
        },
    };

    const ts = await compile(content, outputName, {
        $refOptions: {
            resolve: {
                file: false,
                http: resolver,
              },
        },
        customName: (schema, keyNameFromDefinition) => {
            if (schema.title?.startsWith('CityJSON ') || schema.title?.startsWith('CityJSONFeature ')) return outputName;
            return undefined;
        }
    });
    fs.writeFileSync(path.join(baseDir, outputFile), ts);
    console.log(`Generated ${outputFile}`);
}

const imports = Object.values(cityJsonEntries).concat(Object.values(cityJsonFeatureEntries))
    .map(name => `import type * as ${name} from './${name}.d.ts';`).join("\n");


const exports = [
    genExport("CityJSON", Object.values(cityJsonEntries)),
    genCityJsonVersionExport("1"),
    genCityJsonVersionExport("1_0"),
    genCityJsonVersionExport("1_1"),
    genCityJsonVersionExport("2"),
    genCityJsonVersionExport("2_0"),
    genExport("CityJSONFeature", Object.values(cityJsonFeatureEntries)),
    genCityJsonFeatureVersionExport("1"),
    genCityJsonFeatureVersionExport("1_1"),
    genCityJsonFeatureVersionExport("2"),
    genCityJsonFeatureVersionExport("2_0"),
].join("\n");

fs.writeFileSync(path.join(baseDir, "index.d.ts"), `
${imports}

${exports}
`);

console.log(`Generated index.d.ts`);
